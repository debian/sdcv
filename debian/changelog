sdcv (0.5.5-3) unstable; urgency=medium

  * Add build-essential to autopkgtest deps.

 -- Andrey Rakhmatullin <wrar@debian.org>  Mon, 10 Feb 2025 16:26:39 +0500

sdcv (0.5.5-2) unstable; urgency=medium

  * Adopt the package (Closes: #1085826).
  * Switch to dh-sequence-zz-debputy.
  * Add autopkgtests.
  * Annotate B-D: jq with <!nocheck>.
  * Update debian/clean.

 -- Andrey Rakhmatullin <wrar@debian.org>  Sat, 08 Feb 2025 21:00:06 +0500

sdcv (0.5.5-1) unstable; urgency=medium

  * QA upload.
  * Orphan the package since the package maintainer has retired.
  * New upstream release. (Closes: #978132)
  * debian/watch: Properly monitor upstream GitHub tags.
  * debian/control: Bump Standards-Version to 4.7.0.
  * debian/control: Bump debhelper-compat to v13.
  * debian/patches: Dropped, not needed anymore.
  * debian/rules: Allow -fpermissive to circumvent FTBFS. See #1078404.
  * debian/rules: Disable parallel build to avoid flaky build failure.
  * debian/patches/:
    + 0001-Disable-t_list-test-due-to-need-of-installed-sdcv.patch:
      Add patch to disable t_list test, this test needs a working sdcv
      installation and thus is not suitable for post-build test.

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

 -- Boyuan Yang <byang@debian.org>  Tue, 22 Oct 2024 13:57:06 -0400

sdcv (0.5.2-2) unstable; urgency=medium

  * Add patch to fix FTBFS on big endian machines (Closes: #881221).

 -- Michal Čihař <nijel@debian.org>  Fri, 17 Nov 2017 15:58:22 +0100

sdcv (0.5.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 3.9.8.
  * Move packaging to Git, adjust Vcs urls.
  * Update upstream homepage.
  * Bump standards to 4.1.1.
  * Run full testsuite, add jq to build depends for that.

 -- Michal Čihař <nijel@debian.org>  Wed, 08 Nov 2017 14:35:09 +0100

sdcv (0.5.0~beta4-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Wed, 29 Oct 2014 10:39:08 +0100

sdcv (0.5.0~beta2-3) unstable; urgency=low

  * Adjust Vcs-Svn URL.
  * Bump standards to 3.9.6.
  * Fix debian/copyright.

 -- Michal Čihař <nijel@debian.org>  Mon, 06 Oct 2014 10:06:20 +0200

sdcv (0.5.0~beta2-2) unstable; urgency=low

  * Explicitly build depend on g++ (>=4:4.8) for c++11 support.

 -- Michal Čihař <nijel@debian.org>  Thu, 31 Oct 2013 09:41:03 +0100

sdcv (0.5.0~beta2-1) unstable; urgency=low

  * Update watch file.
  * New upstream release (Closes: #721282).
    - All patches incorporated upstream.
    - Should fix searching for frequent words (Closes: #680281).
  * Use canonical VCS URLs in debian/control.
  * Adjust debian/rules to new CMake based build system.

 -- Michal Čihař <nijel@debian.org>  Tue, 10 Sep 2013 09:17:02 +0200

sdcv (0.4.2-17) unstable; urgency=low

  * Bump standards to 3.9.4.
  * Use debhelper 9 and use hardening flags thanks to it.

 -- Michal Čihař <nijel@debian.org>  Mon, 17 Jun 2013 10:11:00 +0200

sdcv (0.4.2-16) unstable; urgency=low

  * Bump standards to 3.9.3.
  * Add French translation.
  * Adjust debian/copyright to current copyright format.
  * Update patch headers to match DEP-3.
  * Build with dpkg build flags.

 -- Michal Čihař <nijel@debian.org>  Tue, 26 Jun 2012 13:12:55 +0200

sdcv (0.4.2-15) unstable; urgency=low

  * Update gettext requirement to get newer m4 file (Closes: #572509).

 -- Michal Čihař <nijel@debian.org>  Thu, 04 Mar 2010 20:07:02 +0100

sdcv (0.4.2-14) unstable; urgency=low

  * Build depend on autopoint (Closes: #572480).
  * Bump standards to 3.8.4.

 -- Michal Čihař <nijel@debian.org>  Thu, 04 Mar 2010 16:40:52 +0100

sdcv (0.4.2-13) unstable; urgency=low

  * Convert to 3.0 (quilt) source format.
  * Simplify debian/rules.
  * Fix typo in man page.

 -- Michal Čihař <nijel@debian.org>  Sun, 03 Jan 2010 14:08:36 +0100

sdcv (0.4.2-12) unstable; urgency=low

  * Build depend on libreadline-dev only, no libreadline5-dev
    (Closes: #553848).
  * Bump standards to 3.8.3.

 -- Michal Čihař <nijel@debian.org>  Mon, 02 Nov 2009 08:47:06 +0100

sdcv (0.4.2-11) unstable; urgency=low

  * Simplify debian/rules by using overrides, --with-quilt and debian/clean.
  * Regenerate autoconf files during build (Closes: #534814).

 -- Michal Čihař <nijel@debian.org>  Mon, 29 Jun 2009 10:25:24 +0200

sdcv (0.4.2-10) unstable; urgency=low

  * Build depend on autotools-dev to update config.{sub,guess}.
  * Bump standards to 3.8.2 (no changes needed).
  * Avoid duplicating section in debian/control.
  * Point to GPL-2 in debian/copyright.
  * Improve description a bit.

 -- Michal Čihař <nijel@debian.org>  Wed, 24 Jun 2009 11:37:20 +0200

sdcv (0.4.2-9) unstable; urgency=low

  * Switch Vcs-Browser to viewsvn.
  * Fix FTBFS with GCC 4.4 (Closes: #505344).
  * Document all Debian patches.
  * Add ${misc:Depends} to deps.

 -- Michal Čihař <nijel@debian.org>  Fri, 19 Dec 2008 14:09:09 +0100

sdcv (0.4.2-8) unstable; urgency=low

  * Fix FTBFS if built twice (Closes: #503031).
  * Unfuzzy string in Slovak translation (caused by another patch and the
    translation is not really fuzzy).

 -- Michal Čihař <nijel@debian.org>  Wed, 22 Oct 2008 08:02:56 +0200

sdcv (0.4.2-7) unstable; urgency=low

  * Properly typecast to avoid problems with signed char.
  * Fix format chars to avoid possible problems on 64-bit arches.
  * Properly handle gzread return values.

 -- Michal Čihař <nijel@debian.org>  Mon, 20 Oct 2008 11:22:28 +0200

sdcv (0.4.2-6) unstable; urgency=low

  * Fix unalligned access to internal buffer while reading dictionary index
    (Closes: #500921).

 -- Michal Čihař <nijel@debian.org>  Mon, 06 Oct 2008 14:07:45 +0200

sdcv (0.4.2-5) unstable; urgency=low

  * Make sdcv accept $HOME as home directory (Closes: #500921).

 -- Michal Čihař <nijel@debian.org>  Fri, 03 Oct 2008 09:18:52 +0200

sdcv (0.4.2-4) unstable; urgency=low

  * Create fake home directory to make testing possible (Closes: #500318).

 -- Michal Čihař <nijel@debian.org>  Mon, 29 Sep 2008 08:46:18 +0200

sdcv (0.4.2-3) unstable; urgency=low

  * New maintainer (Closes: #480557).
  * Maintain package in collab-maint, add Vcs-* headers.
  * Convert to debhelper 7.
  * Use quilt to apply all patches.
  * Acknowledge NMUs, thanks to all who helped (Closes: #470985, #474819,
    #450040, #417687, #441754).
  * Fix running of testsuite with dash as /bin/sh.
  * Added Czech translation.
  * Run explicitely update-gmo to regenerate translations.
  * Fix typo in english translation.
  * Fix errors in uk man page.
  * All patches were reported upstream.
  * Update to standards 3.8.0.
  * Use machine-interpretable debian/copyright.

 -- Michal Čihař <nijel@debian.org>  Wed, 24 Sep 2008 11:25:30 +0200

sdcv (0.4.2-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * FTBFS: libwrapper.cpp:55: error: 'strchr' was not declared in this
    scope (Closes: #474819)
  * debian/control:
    - Bump Standards-Version to 3.7.3.
    - Use Homepage: field for upstream URL.
  * Update debian/watch file to use SourceForge-specific syntax (Closes:
    450040)

 -- Chris Lamb <chris@chris-lamb.co.uk>  Sat, 12 Apr 2008 01:10:51 +0100

sdcv (0.4.2-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC 4.3 (Closes: #417687).
  * Update Slovak translation (Closes: #441754).

 -- Luk Claes <luk@debian.org>  Fri, 14 Mar 2008 22:22:31 +0000

sdcv (0.4.2-2) unstable; urgency=low

  * Apply patch for AMD64 build by Matej Vela <vela@debian.org>
    (Closes: #369702)
  * Apply patch for pango markup support (Closes: #365324)

 -- Cai Qian <caiqian@debian.org>  Sat, 10 June 2006 19:33:09 +0100

sdcv (0.4.2-1) unstable; urgency=low

  * New upstream release
  * Modify watch file
  * Use new policy

 -- Cai Qian <caiqian@debian.org>  Wed, 31 May 2006 00:57:09 +0100

sdcv (0.4-3) unstable; urgency=low

  * Correct watch file format
  * Add readline option in debian/rules

 -- Cai Qian <caiqian@debian.org>  Sat, 22 Oct 2005 20:08:00 +0100

sdcv (0.4-2) unstable; urgency=low

  * Build with libreadline support
  * Add watch file

 -- Cai Qian <caiqian@debian.org>  Thu, 20 Oct 2005 21:10:00 +0100

sdcv (0.4-1) unstable; urgency=low

  * New upstream release
  * SDCV did not provide libsdcv and libsdcv-dev any more
  * Corrected FSF address

 -- Cai Qian <caiqian@debian.org>  Tue, 27 Sep 2005 13:03:00 +0100

sdcv (0.3.4-1) unstable; urgency=low

  * New upstream release.
  * Now SDCV is using autotools build system.
  * Split SDCV into sdcv, libsdcv3 and libsdcv-dev.
  * Add Chinese po file.
  * Sponsored by Brian Nelson.

 -- Cai Qian <caiqian@gnome.org>  Mon, 21 Jan 2005 09:49:03 +0800

sdcv (0.1-1) unstable; urgency=low

  * Initial Release. (Closes: #250532)
  * Sponsored by Brian Nelson <pyro@debian.org>

 -- Cai Qian <caiqian@gnome.org>  Mon, 15 Nov 2004 23:12:29 +0800
